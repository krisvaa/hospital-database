package no.ntnu.idatt2001.foldertask1.hospitaldatabase;

import no.ntnu.idatt2001.foldertask1.hospitaldatabase.exception.RemoveException;
import no.ntnu.idatt2001.foldertask1.hospitaldatabase.healthpersonal.Nurse;
import no.ntnu.idatt2001.foldertask1.hospitaldatabase.healthpersonal.doctor.Doctor;
import no.ntnu.idatt2001.foldertask1.hospitaldatabase.healthpersonal.doctor.GeneralPractitioner;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.Nested;

import static org.junit.jupiter.api.Assertions.*;

public class DepartmentTest {

    static Department department;

    static Employee[] testEmployees;
    static Patient[] testPatients;

    @BeforeAll
    static void initializeTest() {
        department = new Department("Test Department");

        testEmployees = new Employee[]{
                new GeneralPractitioner("Geir", "Andersen", "11115012345"),
                new Nurse("Kaja", "Halvorsen", "11115012346"),
                new Employee("Olav", "Jensen", "11115012347")
        };

        testPatients = new Patient[]{
                new Patient("Kristian", "Høiberg", "11115012351"),
                new Patient("Erna", "Solberg", "11115012352"),
                new Patient("Camilla", "Jensen", "11115012353")
        };

        //Add test employees
        for(Employee e : testEmployees) {department.addEmployee(e);}

        //Add test patients
        for(Patient p : testPatients) {department.addPatient(p);}
        assertTrue(true);
    }



    @Nested
    @DisplayName("[POSITIVIE] Remove existing patient and existing employee from department")
    class RemovePersonSuccess {
        @Test
        void testRemoveSuccessEmployee() {
            Employee existingEmployee = testEmployees[1];

            try {
                department.remove(existingEmployee);
                assertTrue(true);
            } catch (RemoveException e) {
                e.printStackTrace();
                assertTrue(false);
            }
        }

        @Test
        void testRemoveSuccessPatient() {
            Patient existingPatient = testPatients[1];

            try {
                department.remove(existingPatient);
                assertTrue(true);
            } catch (RemoveException e) {
                e.printStackTrace();
                assertTrue(false);
            }
        }
    }

    @Nested
    @DisplayName("[NEGATIVE] Attempt to remove non-existing patient and employee, and a null-pointer item from department")
    class RemovePersonUnsuccessful {

        @Test
        void testRemoveUnsuccessfulEmployee() {
            Employee newEmployee = new Employee("Conrad", "Murray", "432252523");

            try {
                department.remove(newEmployee);
                assertTrue(false);
            } catch (RemoveException e) {
                e.printStackTrace();
                assertTrue(true);
            }
        }

        @Test
        void testRemoveUnsuccessfulPatient() {
            Patient newPatient = new Patient("Micheal", "Jackson", "001122333231");

            try {
                department.remove(newPatient);
                assertTrue(false);
            } catch (RemoveException e) {
                e.printStackTrace();
                assertTrue(true);
            }
        }

        @Test
        void testRemoveNull() {
            Patient person = null;

            try {
                department.remove(person);
                assertTrue(false);
            } catch (RemoveException e) {
                e.printStackTrace();
                assertTrue(true);
            }
        }
    }

}
