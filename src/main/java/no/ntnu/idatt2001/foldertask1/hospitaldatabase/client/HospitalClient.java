package no.ntnu.idatt2001.foldertask1.hospitaldatabase.client;

import no.ntnu.idatt2001.foldertask1.hospitaldatabase.Department;
import no.ntnu.idatt2001.foldertask1.hospitaldatabase.Employee;
import no.ntnu.idatt2001.foldertask1.hospitaldatabase.Hospital;
import no.ntnu.idatt2001.foldertask1.hospitaldatabase.Patient;
import no.ntnu.idatt2001.foldertask1.hospitaldatabase.exception.RemoveException;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class HospitalClient {

    private Hospital hospital;

    public static void main(String[] args) {
        HospitalClient client = new HospitalClient();

        client.start();
    }

    public HospitalClient() {
        this.hospital = new Hospital("St. Olavs hospital");

        //Fills hospital object with departments and people
        HospitalTestData.fillRegisterWithTestData(hospital);
    }

    public void start() {
        //Prints overview of the hospital-object from the start
        System.out.println("How the hospital looks from the start: ");
        displayHospital(hospital);

        //Creates a iterator of the departments, and takes the saves the first department in a variable
        Iterator<Department> i = hospital.getDepartments().iterator();
        Department dep = i.next();

        //Copied employee information from HospitalTestData, this is the employee to be removed
        Employee e = new Employee("Rigmor", "Mortis", "3");
        System.out.println("Trying to remove " + e + " from the Akutten department...");

        wait(2500);

        try {
            dep.remove(e);
        } catch (RemoveException removeException) {
            removeException.printStackTrace();
        }

        System.out.println("How the hospital looks after removing Rigmor:  \n");
        wait(2500);
        displayHospital(hospital);
        wait(2500);

        //Create non-existing patient for testing
        Patient p = new Patient("Tulle", "Pasient", "321");

        System.out.println("Trying to remove " + p + " from the Akutten department... (Which does not exist)");
        wait(2500);

        try {
            dep.remove(p);
        } catch (RemoveException removeException) {
            System.out.println("Woops... Seems like that didnt work... The patient does not exist. We got a exception of the type " + removeException.getClass());
            System.out.println("\nHere we could ask the user to enter the patient information again... :)\n");
            wait(2500);
        }

        System.out.println("The hospital looks like this, after we tried to remove a non-existing user: ");
        displayHospital(hospital);

        wait(5000);
        System.out.println("That was it for the demo! Hope you enjoyed :)");

    }

    /**
     * Displays hospital in System.out (console)
     *
     * @param hospital Hospital to be displayed
     */
    public void displayHospital(Hospital hospital) {
        String name = hospital.getHospitalName();
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("Departments and people at hospital " + name + ":\n");

        Iterator<Department> i = hospital.getDepartments().iterator();

        while (i.hasNext()) {
            Department d = i.next();
            displayDepartment(d);
        }

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
    }

    /**
     * Displays a department in System.out (console)
     *
     * @param department Department to be displayed
     */
    public void displayDepartment(Department department) {
        String name = department.getDepartmentName();

        Set<Employee> employees = department.getEmployees();
        Set<Patient> patients = department.getPatients();

        System.out.println("Department " + name + ":");


        Iterator<Employee> emtIt = employees.iterator();
        int numOfEmployees = employees.size();
        System.out.println("\nEmployees" + "(" + numOfEmployees +"): ");

        while (emtIt.hasNext()) {
            Employee e = emtIt.next();
            System.out.println(e);
        }


        Iterator<Patient> patIt = patients.iterator();
        int numOfPatients = patients.size();
        System.out.println("\nPatients" + "(" + numOfPatients +"): ");

        while (patIt.hasNext()) {
            Patient p = patIt.next();
            System.out.println(p);
        }

        System.out.println("\n");

    }

    /**
     * Used to halt the program execution, so we can implements pauses between actions
     *
     * @param milliseconds Milliseconds to pause
     */
    private void wait(int milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException interruptedException) {
            interruptedException.printStackTrace();
        }
    }

}
