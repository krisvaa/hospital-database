package no.ntnu.idatt2001.foldertask1.hospitaldatabase.healthpersonal.doctor;

import no.ntnu.idatt2001.foldertask1.hospitaldatabase.Patient;

public class GeneralPractitioner extends Doctor {

    public GeneralPractitioner(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }


    @Override
    public void setDiagnosose(Patient patient, String diagnosis) {
        patient.setDiagnosis(diagnosis);
    }

    @Override
    public String toString() {
        return "GeneralPractitioner, " + super.toString();
    }
}
