package no.ntnu.idatt2001.foldertask1.hospitaldatabase;

public class Patient extends Person implements Diagnosable {

    private String diagnosis = "";

    public Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    public String getDiagnose() {
        return diagnosis;
    }

    /**
     *
     * Method for setting a diagnose on Patient
     *
     * @param diagnosis Diagnose description to be set
     */
    @Override
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    @Override
    public String toString() {
        return "Patient {" +
                "diagnosis='" + getDiagnose() + '\'' +
                "} " + super.toString();
    }
}
