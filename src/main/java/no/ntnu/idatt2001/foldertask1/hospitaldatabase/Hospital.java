package no.ntnu.idatt2001.foldertask1.hospitaldatabase;

import java.util.HashSet;

public class Hospital {

    private final String hospitalName;
    private HashSet<Department> departmentList;

    /**
     *
     * Creates new Hospital object
     * A Hospital object can contain a number of different departments
     *
     * @param name Name of the hospital
     */
    public Hospital(String name) {
        this.hospitalName = name;

        departmentList = new HashSet<>();
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public HashSet<Department> getDepartments() {
        return departmentList;
    }

    /**
     *
     * Add new departement to hospital
     *
     * @param d Department to be added
     */
    public void addDepartment(Department d) {
        boolean success = departmentList.add(d);

        if(!success) {
            throw new IllegalArgumentException(
                    "Departement " + d.getDepartmentName() + " already exists in " + this.getHospitalName()
            );
        }
    }

    @Override
    public String toString() {
        return "Hospital{" +
                "hospitalName='" + hospitalName + '\'' +
                ", departmentList=" + departmentList +
                '}';
    }
}
