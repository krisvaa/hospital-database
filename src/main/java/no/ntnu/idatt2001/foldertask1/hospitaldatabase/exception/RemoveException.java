package no.ntnu.idatt2001.foldertask1.hospitaldatabase.exception;

public class RemoveException extends Exception {

    static final long serialVersionUID = 1L;

    /**
     *
     * Exeption to be used when an error occurs while attempting to remove a person from department
     *
     * @param message Information about the nature of the exception
     */

    public RemoveException(String message) {
        super(message);
    }

}
