package no.ntnu.idatt2001.foldertask1.hospitaldatabase;

import no.ntnu.idatt2001.foldertask1.hospitaldatabase.exception.RemoveException;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Department {

    private String departmentName;

    private HashSet<Employee> employeeList;
    private HashSet<Patient> patientList;

    /**
     * Creates a new Department-object.
     * A department object consists of employees and patients.
     *
     * @param name Name of department
     */
    public Department(String name) {
        employeeList = new HashSet<>();
        patientList = new HashSet<>();

        this.departmentName = name;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public Set<Employee> getEmployees() {
        return employeeList;
    }

    /**
     * Adds a new employee to the department
     *
     * @param e Employee to be added
     * @throws IllegalArgumentException If given employee already exists in the department
     */
    public void addEmployee(Employee e) {
        boolean success = employeeList.add(e);

        if(!success) {
            throw new IllegalArgumentException(
                    "Employee " + e.getFirstName() + " " + e.getLastName() + " already exists with SSN " + e.getSocialSecurityNumber()
            );
        }
    }

    public Set<Patient> getPatients() {
        return patientList;
    }

    /**
     * Adds a new patient to the department
     *
     * @param p Patient to be added
     * @throws IllegalArgumentException If given patient already exists in the department
     */
    public void addPatient(Patient p) {
        boolean success = patientList.add(p);

        if(!success) {
            throw new IllegalArgumentException(
                    "Patient " + p.getFirstName() + " " + p.getLastName() + " already exists with SSN " + p.getSocialSecurityNumber()
            );
        }
    }

    /**
     * Removes given Person from department. Checks if person is an employee or patient, and attempts to remove the person from correct list.
     *
     * @param p Person to be removed
     * @throws RemoveException If person was not found as Employee or Patient, or if person does not exist in department as given type.
     */
    public void remove(Person p) throws RemoveException {
        boolean removeEmployee = p instanceof Employee;
        boolean removePatient = p instanceof Patient;

        boolean wasRemoved;

        if(removeEmployee) {
            wasRemoved = employeeList.remove(p);
        } else if(removePatient) {
            wasRemoved = patientList.remove(p);
        } else {
            wasRemoved = false;
        }

        if(!wasRemoved) {
            throw new RemoveException("Unable to remove " + p + " from department " + departmentName);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return Objects.equals(departmentName, that.getDepartmentName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(departmentName);
    }

    @Override
    public String toString() {
        return "Department{" +
                "departmentName='" + departmentName + '\'' +
                ", employeeList=" +  employeeList +
                ", patientList=" + patientList +
                '}';
    }
}
