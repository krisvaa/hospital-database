package no.ntnu.idatt2001.foldertask1.hospitaldatabase;

public interface Diagnosable {

    /**
     *
     * Method for setting a diagnose
     *
     * @param diagnosis Diagnose description to be set
     */
    void setDiagnosis(String diagnosis);

}
