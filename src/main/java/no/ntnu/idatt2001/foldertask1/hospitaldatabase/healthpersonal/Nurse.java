package no.ntnu.idatt2001.foldertask1.hospitaldatabase.healthpersonal;

import no.ntnu.idatt2001.foldertask1.hospitaldatabase.Employee;

public class Nurse extends Employee {

    public Nurse(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString() {
        return "Nurse{" +
                "firstName='" + this.getFirstName() + '\'' +
                ", lastName='" + this.getLastName()  + '\'' +
                ", socialSecurityNumber='" + this.getSocialSecurityNumber()  + '\'' +
                '}';
    }
}
