package no.ntnu.idatt2001.foldertask1.hospitaldatabase.healthpersonal.doctor;

import no.ntnu.idatt2001.foldertask1.hospitaldatabase.*;

public abstract class Doctor extends Employee {

    public Doctor(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Sets given diagnosis to given patient
     *
     * @param patient Patient to receive the diagnosis
     * @param diagnosis The diagnosis to be sat
     */
    public abstract void setDiagnosose(Patient patient, String diagnosis);

}
