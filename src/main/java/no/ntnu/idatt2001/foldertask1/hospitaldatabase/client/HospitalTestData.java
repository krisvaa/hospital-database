package no.ntnu.idatt2001.foldertask1.hospitaldatabase.client;

import no.ntnu.idatt2001.foldertask1.hospitaldatabase.Department;
import no.ntnu.idatt2001.foldertask1.hospitaldatabase.Employee;
import no.ntnu.idatt2001.foldertask1.hospitaldatabase.Hospital;
import no.ntnu.idatt2001.foldertask1.hospitaldatabase.Patient;
import no.ntnu.idatt2001.foldertask1.hospitaldatabase.healthpersonal.Nurse;
import no.ntnu.idatt2001.foldertask1.hospitaldatabase.healthpersonal.doctor.GeneralPractitioner;
import no.ntnu.idatt2001.foldertask1.hospitaldatabase.healthpersonal.doctor.Surgeon;

public final class HospitalTestData {

    private HospitalTestData() {
        //not called
    }

    /**
     * Populates a hospital with test data
     * Two departments Emergency and Children Polyclinic are added, already populated with test- employees and patients.
     *
     * @param hospital Hospital which shall be populated with test-data
     *
     * */

    public static void fillRegisterWithTestData(final Hospital hospital) {

        // Add some departments
        Department emergency = new Department("Akutten");
        emergency.getEmployees().add(new Employee("Odd Even", "Primtallet", "1"));
        emergency.getEmployees().add(new Employee("Huppasahn", "DelFinito", "2"));
        emergency.getEmployees().add(new Employee("Rigmor", "Mortis", "3"));
        emergency.getEmployees().add(new GeneralPractitioner("Inco", "Gnito", "4"));
        emergency.getEmployees().add(new Surgeon("Inco", "Gnito", "5"));
        emergency.getEmployees().add(new Nurse("Nina", "Teknologi", "6"));
        emergency.getEmployees().add(new Nurse("Ove", "Ralt", "7"));

        emergency.getPatients().add(new Patient("Inga", "Lykke", "8"));
        emergency.getPatients().add(new Patient("Ulrik", "Smål", "9"));

        hospital.getDepartments().add(emergency);


        Department childrenPolyclinic = new Department("Barn poliklinikk");
        childrenPolyclinic.getEmployees().add(new Employee("Salti", "Kaffen", "11"));
        childrenPolyclinic.getEmployees().add(new Employee("Nidel V.", "Elvefølger", "12"));
        childrenPolyclinic.getEmployees().add(new Employee("Anton", "Nym", "13"));
        childrenPolyclinic.getEmployees().add(new GeneralPractitioner("Gene", "Sis", "14"));
        childrenPolyclinic.getEmployees().add(new Surgeon("Nanna", "Na", "15"));
        childrenPolyclinic.getEmployees().add(new Nurse("Nora", "Toriet", "16"));

        childrenPolyclinic.getPatients().add(new Patient("Hans", "Omvar", "17"));
        childrenPolyclinic.getPatients().add(new Patient("Laila", "La", "18"));
        childrenPolyclinic.getPatients().add(new Patient("Jøran", "Drebli", "19"));

        hospital.getDepartments().add(childrenPolyclinic);
    }

}
